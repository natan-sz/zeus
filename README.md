# Zeus

## Framework

This is defined by Zeus.idef0 file.

### Model

[Zeus Complete Model](Zeus.png)

### bin

### library

### images

### configuration

### Branching Strategy

## Sprint

The sprint are designed for review, quality control and black box unit testing and system testing.

### Story Point Estimates

| Days | Estimation Guidelines |
|-- | -- |
| 0 | **No effort is required**, or there is some effort required, but there is no business value delivered, so no Points are accumulated for doing the work. An example is a desired behaviour change deriving from the Scrum Retrospective. |
| 1 | ** Extra Small ** Developers feel they understand most requirements and consider it relatively easy. Probably the smallest item in the Sprint and mostly like completed in one day. |
| 2 | ** Small **. A little bot of thought, effort, or problem-solving is required, but the Developer have done this a lot, so they have confidence in the requirements. Or it sounds extra small, but they want to hedge their bet just a bit. |
| 3 | ** Average **. Developers have done this alot; they know what is needs to be done. There may be a few extra step, but that's it. It is doubtful that they will need to research anything. |
| 5 | ** Large **. This is complex work, or the Developer don't do this very often. Most Developers will need assistance from someone else on the team. This is probably one of the largest items that can be completed within a Sprint. |
| 8 | ** Extra Large**. This going to take some time and research and probably more than one developer to complete within two weeks. In addition, Developers need to make several assumptions to increase the risk and could affect getting it **DONE**. |
| 13 | ** Warning **. This is a complex piece of the work with a lot of unknowns and requires multiple assumptions to size. It is too much to complete in one Sprint. Instead, split this into multiple items that can be completed independently. |
| 21 | ** Hazard!!! **. A 21 and 34 reflects too much complexity to be done within one Sprint. It will be need to be refined more. The large size also indicates more risk, assumptions and dependencies involved to complete this item. |
| ? | ** Danger! ** As a developer, we don't want to do this work the way it is currently written. It is very complex and cannot be completed in the time frame of an iteration or Sprint. Perhaps the requirements are so fuzzy that it's rife with danger |

## Tools Dependencies

### [IDEFO-SVG](https://bitbucket.org/chuffedlepton/idef0-svg/src/master/)

This tool is used to define the functional component model for Zeus.

## Quality Standards & Testing

Rules for Quality Standards and Testing

* All rules setup can be checked through Client and/or Server Side Validation

### Client Side

#### Git Hooks

##### pre-commit

* Ensure there are no white-spaces at the end of line or files.
```
exec git diff-index --check --cached $against --
```

##### prepare-commit-msg

Add a "Signed-of-by <Author><author@ecs.co.uk>" to very commit message.

### Server Side
