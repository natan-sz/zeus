# Setting Global Directories
echo "Reading Zeus Project Configuration"
export ZEUS_HOME=$(git rev-parse --show-toplevel)
export ZEUS_BIN="${ZEUS_HOME}/framework/bin"
export ZEUS_IMAGES="${ZEUS_HOME}/framework/images"

export ZEUS_ACCEL="${ZEUS_HOME}/Accelerator"
export ZEUS_IF="${ZEUS_HOME}/Interfaces"
export ZEUS_DATA="${ZEUS_HOME}/Data"
export ZEUS_METHOD_CONFIG="${ZEUS_HOME}/Configuration"

export ZEUS_MODEL="${ZEUS_HOME}/framework/model/Zeus.idef0"

export REPOLINK="https://bitbucket.org/chuffedlepton/zeus/src/Corp"
