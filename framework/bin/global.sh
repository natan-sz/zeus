#!/bin/bash
# Defining the Shell Path and global variables
ZEUS_BIN=$(realpath $0 | xargs dirname)
source ${ZEUS_BIN}/../config/Zeus.sh

# Colours Variables
red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
blue=`tput setaf 4`
pink=`tput setaf 5`
reset=`tput sgr0`

# Screen Printing of the Information Passed
information() {
  echo "${blue}$(date +"%Y-%m-%d %H:%M:%S") -> ${green}$1"
  tput sgr0
}

# Screen Printing of the Warning Passed
warn() {
  echo "${blue}$(date +"%Y-%m-%d %H:%M:%S") -> ${yellow}$1"
  tput sgr0
}

# Show an informative error
error() {
  echo "${blue}$(date +"%Y-%m-%d %H:%M:%S") -> ${red}$1${reset}"
  exit 12
}

