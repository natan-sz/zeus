#!/usr/bin/env bash

# This script sets up zeus for mac

set -e

# Shared functions
pretty_print() {
  printf "\n%b\n" "$1"
}

if [ -n "`$SHELL -c 'echo $ZSH_VERSION'`" ]; then
	SH_RC=~/.zshrc
elif [ -n "`$SHELL -c 'echo $BASH_VERSION'`" ]; then
	SH_RC=~/.bash_profile
else
	pretty_print "Shell not reconginsed"
fi

# Homebrew installation
if ! command -v brew &>/dev/null; then
  pretty_print "Installing Homebrew, an OSX package manager, follow the instructions..." 
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

  if ! grep -qs "recommended by brew doctor" $SH_RC ; then
    pretty_print "Put Homebrew location earlier in PATH ..."
      printf '\n# recommended by brew doctor\n' >> $SH_RC
      printf 'export PATH="/usr/local/bin:$PATH"\n' >> $SH_RC
      export PATH="/usr/local/bin:$PATH"
  fi
else
  pretty_print "You already have Homebrew installed...good job!"
fi

# Homebrew OSX libraries
pretty_print "Updating brew formulas"
  	brew update

pretty_print "Installing GNU core utilities..."
if brew ls --versions coreutils > /dev/null; then
	echo "Coreutils is installed... "
else
	brew install coreutils
	export PATH="$(brew --prefix coreutils)/libexec/gnubin:$PATH"
	printf 'export PATH="$(brew --prefix coreutils)/libexec/gnubin:$PATH"\n' >> $SH_RC
fi

pretty_print "Installing Imagequick..."
	brew install imagemagick

## Download the IDEF0-SVG library
pretty_print "Downloading IDEF0-SVG and adding to path..."
if [ ! -d "IDEF0-SVG" ] ; then
	git clone https://github.com/jimmyjazz/IDEF0-SVG.git
	IDEF_DIR=$(pwd)/IDEF0-SVG/bin
	export PATH="${IDEF_DIR}:$PATH"
	echo "${IDEF_DIR}:$PATH"
	printf "export PATH="${IDEF_DIR}:'$PATH'"\n" >> $SH_RC
else
	echo "IDEF-SVG already present.."
fi

